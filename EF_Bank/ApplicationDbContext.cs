﻿using Kiem_Tra.Models;
using Microsoft.EntityFrameworkCore;


namespace Kiem_Tra.EF_Bank
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<Report> Reports { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer("Server=KHOADAO-LAPTOP;Database=Kiem_TraEF;Trusted_Connection=True;MultipleActiveResultSets=true;Encrypt=False");
        //}

    }

}
