﻿namespace Kiem_Tra.Models
{
    public class Log
    {
        public int Id { get; set; }
        public DateTime LogDatetime { get; set; }
       
        public int TransactionID { get; set; }

        public Transaction? Transaction { get; set; }
    }
}
