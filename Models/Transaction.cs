﻿namespace Kiem_Tra.Models
{
    public class Transaction
    {
        public int TransactionId { get; set; }
        public string? Name { get; set; }
        public int AccountID { get; set; }
        public int EmployeeID { get; set; }


        public Account? Account { get; set; }
        public Employee? Employee { get; set; }

    }
}
