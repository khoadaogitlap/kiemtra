﻿using System.ComponentModel.DataAnnotations;
using System.Security.Principal;

namespace Kiem_Tra.Models
{
    public class Customer
    {
        
        public int Id { get; set; }
        public string? First_name { get; set;}
        public string? Last_name { get; set;}
        public string? Contact_and_address { get; set;}
        public string? UserName { get; set;}
        public string? Password { get; set;}
        

    }
}
